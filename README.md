## Dependecies

You will need either [VMware Workstation](https://www.vmware.com/products/workstation-pro.html), [VirtualBox](https://www.virtualbox.org/), [Parallels](https://www.parallels.com/) or similar installed on your workstation. Vagrant supports a number of [providers](https://developer.hashicorp.com/vagrant/docs/providers). 

[NOTE]

If you're using Vmware reffer to this [document](https://developer.hashicorp.com/vagrant/docs/providers/vmware
) for installation instructions


If you're on a Windows workstation you might need to install [linux bash shell](https://www.onlogic.com/company/io-hub/how-to-enable-bash-for-windows-10-and-11/
)

Vagrant client itself is located [here](https://developer.hashicorp.com/vagrant/downloads?ajs_aid=297b424e-a59d-4ad3-a03a-f38d0058ed98&product_intent=vagrant).

You will also need a [Git client](https://git-scm.com/download/win)

Vagrant Box location (this needs to be stored some where as we're looking at 8-10 GB file)

----

Assuming the above is complete, first we need to add the above mentioned Vagrant Box to the vagrant's local library.

```
$  vagrant box add FILENAME.box --name BOXNAME
```

[NOTE] BOXNAME should be the same as the entry in the Vagrantfile,

    :sql  => {
      :ip      => '192.168.29.33',
      :mem     => '3072',
      :cpus    => '1',
      :box     => 'BOXNAME',
    }

You can check if the box was added by running 
``` vagrant box list```


Once the box is loaded and Vagrant file is updated with the name run

``` 
$ vagrant up 
```

and when the provisioning is done execute ``` vagrant ssh ``` to "ssh" into the box itself. 

To completely destroy the image once you're done run

```
vagrant destroy -f
```

To access PGAdmin plug ``` http://IPADDR:5050 ``` and for Gitlab ```http://IPADDR```,
where IPADDR is the ip configured in the Vagrantfile, as shown below 

```
 :sql  => {
   :ip      => '192.168.29.33', # IPADDR
   :mem     => '3072',
   :cpus    => '1',
   :box     => 'BOXNAME',
}
```

# Access to services

### PGAdmin

Can be found in the `/opt/db-demo/docker-compose.yaml` as well as credentials to the PostgreSQL


### Gitlab

Can be found in `/opt/db-demo/.gitlab_root_password`
  
   
## Moving Parts

Both PGAdmin and PostgreSQL are running in Docker and configured in ```/opt/db-demo/docker-compose.yaml```
   
   