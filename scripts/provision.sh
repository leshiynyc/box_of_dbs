#!/usr/bin/env bash 

#  set -x 

IPADDR="192.168.29.33"
WORK_DIR="/vagrant/FWD"
REPO_DIR="$WORK_DIR/dvdrental"

# make sure $WORK_DIR exists 
if [ !  -d $WORK_DIR  ] ; then 
 mkdir -p $WORK_DIR
fi 

if [ -d $REPO_DIR  ] ; then 
   echo "Previous deployment present"
   echo "Cleaning"
   sudo rm -rf $REPO_DIR && echo "Clean version of the DVDRental Repo is being deployed"
   echo "Waiting for GitLab API to come online"
   until $(curl --output /dev/null --silent --head --fail http://$IPADDR ); do
  	:        # ":" means do nothing 
   done
   echo "Cloning fresh repo" 
   cd $WORK_DIR ; sudo git clone git@$IPADDR:redgate/dvdrental.git 
 else
   if [ !  -d $REPO_DIR  ]; then 
    echo "Waiting for GitLab API to come online"
        until $(curl --output /dev/null --silent --head --fail http://$IPADDR ); do
                :         # ":" means do nothing
        done
     echo "Cloning fresh repo"
     cd $WORK_DIR ; sudo git clone git@$IPADDR:redgate/dvdrental.git
   fi
fi 

 
